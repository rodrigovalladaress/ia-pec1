﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentControl2 : MonoBehaviour {

    [SerializeField]
    private Transform _objective;
    [SerializeField]
    private float _minDistanceFromObjective = 3f;

    private NavMeshAgent _agent;

    private void Awake() {
        _agent = GetComponent<NavMeshAgent>();
        _agent.stoppingDistance = _minDistanceFromObjective;
    }

    void Update() {
        if (_objective) {
            _agent.destination = _objective.position;
        }
    }
}
