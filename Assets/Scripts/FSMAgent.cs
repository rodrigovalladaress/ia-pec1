﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMAgent : MonoBehaviour {

    [SerializeField]
    private float _nearDistance = 2f;
    [SerializeField]
    private float _farDistance = 4f;

    private Transform _objective;
    private AgentState _currentState;
    private AgentState _chaseState;
    private AgentState _randomMovementState;
    private float _distance;
    private AgentControl3 _agent;

    public AgentState ChaseState {
        get {
            return _chaseState;
        }
        set {
            _chaseState = value;
        }
    }

    public AgentState RandomMovementState {
        get {
            return _randomMovementState;
        }
        set {
            _randomMovementState = value;
        }
    }

    public float NearDistance {
        get {
            return _nearDistance;
        }
        set {
            _nearDistance = value;
        }
    }

    public float FarDistance {
        get {
            return _farDistance;
        }
        set {
            _farDistance = value;
        }
    }

    public float Distance {
        get {
            return _distance;
        }
        set {
            _distance = value;
        }
    }

    public AgentState CurrentState {
        get {
            return _currentState;
        }
        set {
            _currentState = value;
        }
    }

    public Transform Objective {
        get {
            return _objective;
        }
        set {
            _objective = value;
        }
    }

    public AgentControl3 Agent {
        get {
            return _agent;
        }
        set {
            _agent = value;
        }
    }

    void Awake () {
        _chaseState = new ChaseState(this);
        _randomMovementState = 
            new RandomMovementState(this);
        _currentState = _chaseState;
	}
	
	void Update () {
        _distance = Vector3.Distance(transform.position,
            _objective.position);
        _currentState.UpdateState();
	}

}
