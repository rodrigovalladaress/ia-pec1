﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseState : AgentState {

    public ChaseState(FSMAgent fsm) {
        FSM = fsm;
    }

    public override void UpdateState() {
        if(FSM.Distance <= FSM.NearDistance) {
            ToState(FSM.RandomMovementState);
        }
    }

}
