﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AgentState {

    private FSMAgent _fsm;

    protected FSMAgent FSM {
        get {
            return _fsm;
        }
        set {
            _fsm = value;
        }
    }

    protected void ToState(AgentState state) {
        Debug.Log("From " + GetType().Name + " to state " + state.GetType().Name);
        _fsm.CurrentState = state;
    }

    public abstract void UpdateState();

}
