﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovementState : AgentState {

    public RandomMovementState(FSMAgent fsm) {
        FSM = fsm;
    }

    public override void UpdateState() {
        if(FSM.Distance >= FSM.FarDistance) {
            ToState(FSM.ChaseState);
        }
    }
}
