﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentControl3 : MonoBehaviour {

    private const int MAX_TRIES = 50;
    private const int WALKABLE_MASK = 1;
    private const float EPSILON = 0.2f;

    [SerializeField]
    private Transform _objective;
    [SerializeField]
    private float _minRandomRange = 2f;
    [SerializeField]
    private float _maxRandomRange = 4f;
    [SerializeField]
    private GameObject _randomPointPrefab;
    [SerializeField]
    private Transform _randomPointsParent;

    private NavMeshAgent _agent;
    private FSMAgent _fsm;
    private float _y = float.PositiveInfinity;
    private GameObject _randomObjetive;
    private Vector3 _randomObjetivePosition = Vector3.zero;

    private int _i = 0;

    private void Awake() {
        _agent = GetComponent<NavMeshAgent>();
        _fsm = GetComponent<FSMAgent>();
        _fsm.Objective = _objective;
        _fsm.Agent = this;
    }

    void Update() {
        if(_y == float.PositiveInfinity) {
            _y = transform.position.y;
        }
        if (_objective) {
            Vector3 destination;
            if(_fsm.CurrentState == _fsm.ChaseState) {
                destination = _objective.position;
                _randomObjetivePosition = Vector3.zero;
                if(_randomObjetive) {
                    DeactivateLastRandomObjective();
                }
            } else {
                if(_randomObjetivePosition == Vector3.zero  
                    || Near(transform.position, _randomObjetivePosition)) {
                    Vector3 randomPoint;
                    bool canUseRandomPoint = false;
                    NavMeshHit hit;
                    int nTry = 0;
                    do {
                        randomPoint = transform.position 
                            + (Random.insideUnitSphere * _maxRandomRange);
                        randomPoint = new Vector3(randomPoint.x + _minRandomRange, _y,
                            randomPoint.z + _minRandomRange);
                        canUseRandomPoint = NavMesh.SamplePosition(randomPoint, 
                            out hit, _agent.height * 2, WALKABLE_MASK);
                        if(Vector3.Distance(randomPoint, _objective.position) 
                            <= (_fsm.NearDistance + EPSILON)) {
                            canUseRandomPoint = false;
                        }
                        nTry++;
                    }
                    while (nTry < MAX_TRIES && !canUseRandomPoint);
                    if (canUseRandomPoint) {
                        _randomObjetivePosition = hit.position;
                    } else {
                        _randomObjetivePosition = transform.position;
                    }
                    if (_randomObjetive) {
                        DeactivateLastRandomObjective();
                    }
                    _randomObjetive = Instantiate(_randomPointPrefab,
                        _randomObjetivePosition, 
                        Quaternion.identity);
                    _randomObjetive.name = _i.ToString();
                    _randomObjetive.transform.parent = _randomPointsParent;
                    _i++;
                }
                destination = _randomObjetivePosition;
            }
            _agent.destination = destination;
        }
    }

    private void DeactivateLastRandomObjective() {
        float xScale = _randomObjetive.transform.localScale.x / 2f;
        float yScale = _randomObjetive.transform.localScale.y;
        float zScale = _randomObjetive.transform.localScale.z / 2f;
        _randomObjetive.GetComponent<Renderer>().material.color = Color.grey;
        _randomObjetive.transform.localScale = new Vector3(xScale, yScale, zScale);
        _randomObjetive = null;
    }

    private bool Near(Vector3 a, Vector3 b) {
        return Vector3.Distance(a, b) <= EPSILON;
    }

}