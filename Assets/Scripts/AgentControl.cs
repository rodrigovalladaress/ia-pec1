﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentControl : MonoBehaviour {

    [SerializeField]
    private Transform _destination;

    private NavMeshAgent _agent;

    private void Awake() {
        _agent = GetComponent<NavMeshAgent>();
    }

    void Start () {
		if(_destination) {
            _agent.destination = _destination.position;
        }
	}
}
